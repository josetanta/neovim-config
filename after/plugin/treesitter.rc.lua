local status, ts = pcall(require, 'nvim-treesitter.configs')
if (not status) then return end

local sInstaller, installers = pcall(require, 'nvim-treesitter.install')
if (not sInstaller) then return end

-- Optional Features
installers.compilers = { 'gcc', 'zig' }
-- installers.compilers = { 'zig' }
-- installers.prefer_git = true

ts.setup {
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = true,
	},
	indent = {
		enable = true,
	},
	ensure_installed = {
		'typescript',
		'tsx',
		'lua',
		'json',
		-- 'css',
		'javascript',
		-- 'python',
		'go',
		'gomod',
		-- 'vue',
		-- 'ruby',
		--'graphql',
		-- 'html',
		'dockerfile',
		-- 'c',
		'markdown_inline',
		'markdown',
	},
	autotag = {
		enable = true
	},
	auto_install = false,
}
