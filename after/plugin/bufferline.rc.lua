local status, bufferline = pcall(require, 'bufferline')

if (not status) then
	return
end

bufferline.setup {
	options = {
		mode = 'tabs',
		separator_style = 'slant',
		always_show_bufferline = false,
		show_buffer_close_icons = false,
		show_close_icon = false,
		color_icons = false,
	},
	highlights = {
		-- separator = {
		--	fg = '#073642',
		--	bg = '#002b36'
		--	},
		separator_selected = {
			fg = '#2C3333',
		},
		background = {
			fg = '#A5C9CA',
			bg = '#2C3333'
		},
		buffer_selected = {
			fg = '#E7F6F2',
			bg = '#395B64',
			underline = true,
			bold = true
		},
		fill = {
			bg = '#2C3333',
			color_icons = true
		}
	},
}

vim.api.nvim_set_keymap('n', '<Tab>', '<cmd>BufferLineCycleNext<cr>', {})
vim.api.nvim_set_keymap('n', '<S-Tab>', '<cmd>BufferLineCyclePrev<cr>', {})
