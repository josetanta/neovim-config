local status, todo = pcall(require, 'todo')

if (not status) then
	return
end

todo.setup()
