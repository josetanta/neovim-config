local status, saga = pcall(require, 'lspsaga')


if (not status) then
	return
end

saga.setup {
	--server_filetype_map = {}
	ui = {
		border = 'rounded'
	},
	symbol_in_winder = {
		enable = false
	},
	lightbulb = {
		enable = false
	},
	outline = {
		layout = 'float'
	}
}

local diagnostic = require("lspsaga.diagnostic")

local opts = {
	noremap = true,
	silent = true
}

vim.keymap.set('n', '<C-j>', '<cmd>Lspsaga diagnostic_jump_next<CR>', opts)
vim.keymap.set('n', 'K', '<cmd>Lspsaga hover_doc<CR>', opts)
vim.keymap.set('n', 'gd', '<cmd>Lspsaga finder<CR>', opts)
vim.keymap.set('n', 'gt', '<cmd>Lspsaga goto_type_definition<CR>', opts)
vim.keymap.set('n', 'gl', '<cmd>Lspsaga show_line_diagnostics<CR>', opts)

-- vim.keymap.set('i', '<C-k>', '<cmd>Lspsaga signature_help<cr>', opts) -- command not found
vim.keymap.set('i', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts) -- command not found

vim.keymap.set('n', 'gp', '<cmd>Lspsaga peek_definition<CR>', opts)
vim.keymap.set('n', 'gr', '<cmd>Lspsaga rename<CR>', opts)

-- code actionv
vim.keymap.set({ "n", "v" }, "<leader>ca", "<cmd>Lspsaga code_action<CR>")
