local status, packer = pcall(require, 'packer')

if (not status) then
	print('Packer is not installed')
	return
end


vim.cmd [[packadd packer.nvim]]

packer.startup(function(use)
	use 'wbthomason/packer.nvim'
	--  Themes
	use 'navarasu/onedark.nvim' --  theme onedark
	use {                      -- snippet
		'L3MON4D3/LuaSnip',
		tag = 'v2.*',
	}

	use 'nvim-lualine/lualine.nvim'   -- statusline
	use 'kyazdani42/nvim-web-devicons' -- File icons

	use 'onsails/lspkind-nvim'        -- vscode-like pictograms

	use 'hrsh7th/cmp-buffer'          -- nvim-cmp source for buffer words
	use 'hrsh7th/cmp-nvim-lsp'        -- nvim-cmp source for neovims built LSP
	use 'hrsh7th/nvim-cmp'            -- Completion


	use {
		'nvim-treesitter/nvim-treesitter',
		run = function()
			require('nvim-treesitter.install').update({ with_async = true })
		end,
	}
	use 'jose-elias-alvarez/null-ls.nvim' --	
	use 'MunifTanjim/prettier.nvim'      --  Prettier plugin for Neovim

	use 'neovim/nvim-lspconfig'          -- LSP
	use 'williamboman/mason.nvim'
	use 'williamboman/mason-lspconfig.nvim'

	use 'windwp/nvim-autopairs'
	use 'windwp/nvim-ts-autotag'

	-- Saga
	-- use 'glepnir/lspsaga.nvim' -- LSP UI's
	use 'nvimdev/lspsaga.nvim'
	-- after = 'nvim-lspconfig',

	-- Fuzzy finder
	use 'nvim-lua/plenary.nvim' -- Common utilities
	use {
		'nvim-telescope/telescope.nvim',
		tag = '0.1.3'
	}

	use 'nvim-telescope/telescope-file-browser.nvim'

	use { 'akinsho/nvim-bufferline.lua',
		tag = 'v3.*',
		requires = 'nvim-tree/nvim-web-devicons',
	}

	use 'norcalli/nvim-colorizer.lua' -- colorizer hex and rgba

	use {
		'AmeerTaweel/todo.nvim',
		requires = 'nvim-lua/plenary.nvim',
	}
end)
