vim.cmd("autocmd!")

vim.scriptencoding = 'utf-8'
vim.opt.encoding = 'utf-8'
vim.opt.fileencoding = 'utf-8'

vim.opt.spell = false
vim.opt.spelllang = 'en'
-- vim.opt.spellsuggest = { 'best', 9 }

vim.wo.number = true

-- vim.opt.ai = true -- Auto indent
-- vim.opt.si = true -- Smart indent
vim.opt.autoindent = true
vim.opt.smartindent = true

vim.opt.relativenumber = true
vim.opt.showmode = false
vim.opt.mouse = 'n'
vim.opt.termguicolors = true
vim.opt.title = true
vim.opt.autoindent = true
vim.opt.hlsearch = true
vim.opt.backup = false
vim.opt.showcmd = true
vim.opt.expandtab = true
vim.opt.cmdheight = 1
vim.opt.laststatus = 2
vim.opt.scrolloff = 10
vim.opt.shell = 'powershell'
vim.opt.backupskip = '~/AppData/Local/nvim/tmp/*'
vim.opt.inccommand = 'split'
vim.opt.ignorecase = true
vim.opt.smarttab = true
vim.opt.breakindent = true -- indent
vim.opt.shiftwidth = 2
vim.opt.tabstop = 2
vim.opt.wrap = false         -- No Wrap lines
vim.opt.backspace = { 'start', 'eol', 'indent' }
vim.opt.path:append { '**' } -- Fingingg files - Search down into subfolders
vim.opt.wildignore:append {
	'*/node_modules/*',
	'*/.git/*'
}

-- Undercurl
vim.cmd([[let &t_Cs = "\e[4:3m"]])
vim.cmd([[let &t_Ce = "\e[4:0m"]])
-- but this doesn't work on

-- Turn off paste mode when leaving insert
-- vim.api.nvim_create_autocmd('InsertLeave', {
-- pattern = '*',
-- command = 'set nopaste',
-- })

--  Adds
vim.opt.formatoptions:append { 'r' }
