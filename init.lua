require('josetanta.base')
require('josetanta.highlights')
require('josetanta.maps')
require('josetanta.plugins')

local has = vim.fn.has
local is_linux = has "unix"
local is_win = has "win32"

if is_linux == 1 then
  require('josetanta.linux')
end

if is_win == 1 then
  require('josetanta.windows')
end
